//
//  AppDelegate.h
//  DribbbleApp
//
//  Created by Felipe Henrique Santolim on 4/14/15.
//  Copyright (c) 2015 Felipe Henrique Santolim. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

