//
//  DetailShotViewController.m
//  DribbbleApp
//
//  Created by Felipe Henrique Santolim on 4/14/15.
//  Copyright (c) 2015 Felipe Henrique Santolim. All rights reserved.
//

#import "DetailShotViewController.h"

@interface DetailShotViewController ()

{
    
    __weak IBOutlet UIActivityIndicatorView *mLoadImage;
    __weak IBOutlet UIImageView *imageAvatar;
    __weak IBOutlet UIImageView *imageShots;
    __weak IBOutlet UILabel *lblTitle;
    __weak IBOutlet UILabel *lblBy;
    __weak IBOutlet UITextView *txDescription;
}


@end

@implementation DetailShotViewController



- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    imageAvatar.layer.cornerRadius    =   45;
    imageAvatar.layer.borderColor     =   [UIColor grayColor].CGColor;
    imageAvatar.layer.borderWidth     =   0.2;
    imageAvatar.layer.masksToBounds   =   YES;
    
    lblTitle.text   =   self._shot.title;
    lblBy.text      =   [NSString stringWithFormat:@"by %@",self._shot.player.name];
    
    [imageAvatar sd_setImageWithURL:[NSURL URLWithString:self._shot.player.avatar_url]
                          completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                              
                          }];
    
    [imageShots sd_setImageWithURL:[NSURL URLWithString:self._shot.image_teaser_url]
                         completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                             
                             
                         }];

    if (![self._shot.desc isEqual:[NSNull null]]) {
        txDescription.text  =   [NSString stringWithFormat:@"%@",
                                 [[[[[[[[self._shot.desc
                                         stringByReplacingOccurrencesOfString:@"<p>"         withString:@""]
                                        stringByReplacingOccurrencesOfString:@"</p>"        withString:@""]
                                       stringByReplacingOccurrencesOfString:@"<br>"        withString:@""]
                                      stringByReplacingOccurrencesOfString:@"</br>"       withString:@""]
                                     stringByReplacingOccurrencesOfString:@"</a>"        withString:@""]
                                    stringByReplacingOccurrencesOfString:@"</a>"        withString:@""]
                                   stringByReplacingOccurrencesOfString:@"<href =>"    withString:@""]
                                  stringByReplacingOccurrencesOfString:@"</href>"     withString:@""]];
    }else{
        txDescription.text  =   @"no description";
    }
}



- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    self.title  =   @"Show dribbble shot details";
}



- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

@end
