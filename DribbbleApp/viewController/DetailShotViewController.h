//
//  DetailShotViewController.h
//  DribbbleApp
//
//  Created by Felipe Henrique Santolim on 4/14/15.
//  Copyright (c) 2015 Felipe Henrique Santolim. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Shots.h"
#import <SDWebImage/UIImageView+WebCache.h>

@interface DetailShotViewController : UIViewController

@property (nonatomic) Shots *   _shot;

@end
