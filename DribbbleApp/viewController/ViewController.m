//
//  ViewController.m
//  DribbbleApp
//
//  Created by Felipe Henrique Santolim on 4/14/15.
//  Copyright (c) 2015 Felipe Henrique Santolim. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

{
    NSMutableArray  * mListShots;
    UIAlertView *   loading;
    __weak IBOutlet UITableView *mainTableView;
}

@end



@implementation ViewController


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self.title  =   @"DribbbleApp";
}




- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    mListShots  =   [[NSMutableArray alloc] init];
    mPageNumber =   1;
    
    loading =   [[UIAlertView alloc]
                 initWithTitle:@"Loading shots"
                 message:@"Please, waiting .."
                 delegate:nil
                 cancelButtonTitle:nil
                 otherButtonTitles:nil];
    
    [loading show];
    
    httpService =   [HttpService shared];
    httpService.dribbbleShotsListDelegate   =   self;
    [httpService HttpServiceDribbbleShotsListWithNumberPage:mPageNumber];
    
    detailShotView  =   [self.storyboard instantiateViewControllerWithIdentifier:@"detailShotView"];
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return mListShots.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    CustomShotsTableViewCell *   cell    =   [tableView dequeueReusableCellWithIdentifier:@"cellIdent" forIndexPath:indexPath];
    
    if (cell == nil) {
        
        cell    =   [[CustomShotsTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cellIdent"];
        
    }
    
    NSString    *   shotImage   =   [NSString stringWithFormat:@"%@",
                                     [[mListShots objectAtIndex:indexPath.row] valueForKey:@"image_teaser_url"]];
    
    NSString    *   avatar      =   [NSString stringWithFormat:@"%@",
                                     [[[mListShots objectAtIndex:indexPath.row] valueForKey:@"player"] valueForKey:@"avatar_url"]];
    
    NSString    *   views       =   [NSString stringWithFormat:@"%@ views",
                                     [[mListShots objectAtIndex:indexPath.row] valueForKey:@"views_count"]];
    
    [cell.imageShot sd_setImageWithURL:[NSURL URLWithString:shotImage]
                             completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        
    }];
    
    [cell.imageAvatar sd_setImageWithURL:[NSURL URLWithString:avatar]
                               completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        
    }];
    
    cell.lblViewsCount.text =   views;
    cell.lblNamePlayer.text =   [[mListShots objectAtIndex:indexPath.row] valueForKey:@"title"];
    
    
    
    if (indexPath.row == mListShots.count -1) {
        mPageNumber++;
        [self loadMoreData];
    }
    
    
    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    [SVProgressHUD dismiss];
    
    detailShotView._shot    =   [mListShots objectAtIndex:indexPath.row];
    [self.navigationController pushViewController:detailShotView animated:YES];
}


- (void)loadMoreData
{
    [SVProgressHUD showWithStatus:@"loading more shots"];
    
    httpService =   [HttpService shared];
    httpService.dribbbleShotsListDelegate   =   self;
    [httpService HttpServiceDribbbleShotsListWithNumberPage:mPageNumber];
}




- (void)dribbbleShotsListSuccess:(NSMutableArray *)listShots
{
    [SVProgressHUD dismiss];
    [loading dismissWithClickedButtonIndex:0 animated:YES];
    
    if (listShots.count <= 0) {
        
        UIAlertView *   alert   =   [[UIAlertView alloc] initWithTitle:@""
                                                               message:nil
                                                              delegate:nil
                                                     cancelButtonTitle:@""
                                                     otherButtonTitles:nil];
        [alert show];
        
    }else{
        
        for (Shots *shot in listShots) {
            [self addToArrayShots:mListShots shotObject:shot];
        }
        
        [mainTableView reloadData];
        
    }
}



- (void)dribbbleShotsListError:(NSString *)str
{
    [SVProgressHUD dismiss];
    [loading dismissWithClickedButtonIndex:0 animated:YES];
    
    UIAlertView *   error   =   [[UIAlertView alloc] initWithTitle:@"Error"
                                                           message:str
                                                          delegate:nil
                                                 cancelButtonTitle:@""
                                                 otherButtonTitles:nil];
    [error show];
}



- (void)addToArrayShots:(NSMutableArray *)list shotObject:(Shots *)shotObject
{
    if (![mListShots containsObject:shotObject]) {
        [mListShots addObject:shotObject];
    }
}


@end
