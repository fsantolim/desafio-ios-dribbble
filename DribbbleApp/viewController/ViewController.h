//
//  ViewController.h
//  DribbbleApp
//
//  Created by Felipe Henrique Santolim on 4/14/15.
//  Copyright (c) 2015 Felipe Henrique Santolim. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HttpService.h"
#import "CustomShotsTableViewCell.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "DetailShotViewController.h"
#import "SVProgressHUD.h"

@interface ViewController : UIViewController <DribbbleShotsListDelegate, UITableViewDelegate, UITableViewDataSource>

{
    HttpService                 *   httpService;
    DetailShotViewController    *   detailShotView;
    
    NSInteger   mPageNumber;
}

@end

