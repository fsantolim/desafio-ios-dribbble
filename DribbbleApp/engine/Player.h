//
//  Player.h
//  DribbbleApp
//
//  Created by Felipe Henrique Santolim on 4/14/15.
//  Copyright (c) 2015 Felipe Henrique Santolim. All rights reserved.
//

#import "Jastor.h"

@interface Player : Jastor

@property (nonatomic, strong) NSString  *   name;
@property (nonatomic, strong) NSString  *   avatar_url;
@property (nonatomic, strong) NSString  *   location;

@end
