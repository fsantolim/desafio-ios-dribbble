//
//  Shots.h
//  DribbbleApp
//
//  Created by Felipe Henrique Santolim on 4/14/15.
//  Copyright (c) 2015 Felipe Henrique Santolim. All rights reserved.
//

#import "Jastor.h"
#import <Foundation/Foundation.h>
#import "Player.h"

@interface Shots : Jastor

@property (nonatomic) NSNumber          *   mId;
@property (nonatomic, strong) NSString  *   title;
@property (nonatomic, strong) NSString  *   desc;
@property (nonatomic) NSNumber          *   height;
@property (nonatomic) NSNumber          *   width;
@property (nonatomic) NSNumber          *   likes_count;
@property (nonatomic) NSNumber          *   comments_count;
@property (nonatomic) NSNumber          *   rebounds_count;
@property (nonatomic, strong) NSString  *   url;
@property (nonatomic, strong) NSString  *   short_url;
@property (nonatomic) NSNumber          *   views_count;
@property (nonatomic) NSNumber          *   rebound_source_id;
@property (nonatomic, strong) NSString  *   image_url;
@property (nonatomic, strong) NSString  *   image_teaser_url;
@property (nonatomic, strong) NSString  *   image_400_url;
@property (nonatomic, strong) NSString  *   created_at;
@property (nonatomic) Player            *   player;

@end
