//
//  CustomShotsTableViewCell.h
//  DribbbleApp
//
//  Created by Felipe Henrique Santolim on 4/14/15.
//  Copyright (c) 2015 Felipe Henrique Santolim. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomShotsTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *imageShot;
@property (weak, nonatomic) IBOutlet UIView *boxShot;
@property (weak, nonatomic) IBOutlet UIImageView *imageAvatar;
@property (weak, nonatomic) IBOutlet UILabel *lblNamePlayer;
@property (weak, nonatomic) IBOutlet UILabel *lblViewsCount;

@end
