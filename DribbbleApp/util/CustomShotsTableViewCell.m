//
//  CustomShotsTableViewCell.m
//  DribbbleApp
//
//  Created by Felipe Henrique Santolim on 4/14/15.
//  Copyright (c) 2015 Felipe Henrique Santolim. All rights reserved.
//

#import "CustomShotsTableViewCell.h"

@implementation CustomShotsTableViewCell

- (void)awakeFromNib {
    
    self.imageAvatar.layer.masksToBounds    =   YES;
    self.imageAvatar.layer.cornerRadius     =   32.0;
    self.imageAvatar.layer.borderColor      =   [UIColor grayColor].CGColor;
    self.imageAvatar.layer.borderWidth      =   0.2;
    
}

@end
