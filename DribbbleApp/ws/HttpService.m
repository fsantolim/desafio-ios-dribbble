//
//  HttpService.m
//  DribbbleApp
//
//  Created by Felipe Henrique Santolim on 4/14/15.
//  Copyright (c) 2015 Felipe Henrique Santolim. All rights reserved.
//

#import "HttpService.h"

static HttpService *_instance;

@implementation HttpService


+ (id)shared
{
    if (_instance == nil) {
        
        _instance   =   [[HttpService alloc] init];
        
    }
    
    return _instance;
}


- (void)HttpServiceDribbbleShotsListWithNumberPage:(NSInteger)page
{
    
    NSString    *   url =   [NSString stringWithFormat:@"%@%ld",HTTP_URL_MASTER,(long)page];
    
    NSMutableURLRequest *request = [[AFJSONRequestSerializer serializer]
                                    requestWithMethod:@"GET"
                                    URLString:url
                                    parameters:nil
                                    error:nil];
    
    AFHTTPRequestOperation *op = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    op.responseSerializer = [AFJSONResponseSerializer serializer];
    [op setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary    *   dict    =   responseObject;
        
        if (dict == nil) {
            
            [self.dribbbleShotsListDelegate performSelector:@selector(dribbbleShotsListError:) withObject:@"Error"];
            
        }else{
            
            NSMutableArray  *   listShotsArray  =   [[NSMutableArray alloc] init];
            
            NSArray *   shotsArray  =   [[NSArray alloc] init];
            shotsArray  =   [dict objectForKey:@"shots"];
            
            for (NSDictionary * key in shotsArray) {
                
                Shots   *   shots   =   [[Shots alloc] initWithDictionary:key];

                shots.mId   =   [key valueForKey:@"id"];
                shots.desc  =   [key valueForKey:@"description"];
                
                [listShotsArray addObject:shots];
            }
            
            [self.dribbbleShotsListDelegate performSelector:@selector(dribbbleShotsListSuccess:) withObject:listShotsArray];
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        [self.dribbbleShotsListDelegate performSelector:@selector(dribbbleShotsListError:) withObject:@"Error"];
        
    }];
    [[NSOperationQueue mainQueue] addOperation:op];
    
}


@end
