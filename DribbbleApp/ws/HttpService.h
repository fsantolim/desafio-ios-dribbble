//
//  HttpService.h
//  DribbbleApp
//
//  Created by Felipe Henrique Santolim on 4/14/15.
//  Copyright (c) 2015 Felipe Henrique Santolim. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AFNetworking/AFNetworking.h>
#import "CustomTypes.h"
#import "Shots.h"

@protocol DribbbleShotsListDelegate <NSObject>

@required
- (void)dribbbleShotsListSuccess:(NSMutableArray *)listShots;
- (void)dribbbleShotsListError:(NSString *)str;

@end

@interface HttpService : NSObject

@property (nonatomic) id <DribbbleShotsListDelegate> dribbbleShotsListDelegate;

+ (id)shared;

- (void)HttpServiceDribbbleShotsListWithNumberPage:(NSInteger)page;

@end
